import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StartComponent} from './start/start.component';
import {LayoutComponent} from './layout/layout.component';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  {
    path: 'start',
    component: LayoutComponent,
    children: [
      { path: '', component: StartComponent}
    ]
  },
  {
    path: '',
    component: HomeComponent
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {enableTracing: true})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
