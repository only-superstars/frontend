import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
export class Node {
  id: string;
  node: string;
  constructor(id, node) {
    this.id = id;
    this.node = node;
  }
}

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class StartComponent implements OnInit, OnChanges {
  nodeList: Node[];
  highlighted: any = null;
  changeable: any = null;
  newElem: any = null;
  prevElemValue: string = null;
  exBackendURL: string;

  private getList(): Observable<Node[]> {
    return this.httpClient.get <Node[]>(this.exBackendURL);
  }

  constructor(private httpClient: HttpClient, private ref: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.exBackendURL = environment.backend + '/init';
    this.updateView();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.ref.detectChanges();
  }

  addNew() {
    if (this.highlighted === null && this.changeable === null) {
      this.nodeList.push(new Node(this.nodeList.length + 1, 'New element'));
      this.highlighted = this.nodeList.length;
      this.changeable = this.nodeList.length;
      this.newElem = this.nodeList.length;
    }
  }
  ok() {
    if (this.newElem !== null) {
        this.addPost(this.nodeList.find(x => x.id === this.changeable).node).subscribe();
    } else {
      if (this.changeable !== null) {
        this.editPost(this.nodeList.find(x => x.id === this.changeable)).subscribe();
      }
    }
    this.updateView();
    this.makeNull();
  }
  editPost(element): Observable<string> {
    const editUrl = this.exBackendURL + '/edit';
    return this.httpClient.post <string>(editUrl, element);
  }
  cancel() {
    if (this.newElem !== null) {
      this.nodeList.pop();
    } else {
      if (this.changeable !== null) {
        this.nodeList.find(x => x.id === this.changeable).node = this.prevElemValue;
      }
    }
    this.makeNull();
  }
  makeNull() {
    this.newElem = null;
    this.highlighted = null;
    this.changeable = null;
    this.prevElemValue = null;
  }
  addPost(element): Observable<string> {
    const addUrl = this.exBackendURL + '/new';
    return this.httpClient.post <string>(addUrl, element);
  }
  testFunc(id): boolean {
    return this.createClass(id) === 'changeLi';
  }
  createClass(id): string {
    if (this.highlighted === id && this.changeable === null) {
      return 'selectedLi';
    } else if (this.highlighted === id && this.changeable === id) {
      return 'changeLi';
    } else {
      return 'defaultLi';
    }
  }
  changeClass(id) {
    if (this.changeable === null) {
      if (this.highlighted === null) {
        this.highlighted = id;
      } else {
        if (this.highlighted === id) {
          this.highlighted = null;
        }
      }
    }
  }
  private updateView() {
    this.nodeList = [];
    this.ref.detectChanges();
    this.getList().subscribe(
      x => {
        this.nodeList = x;
        console.log(this.nodeList);
      }
    );
  }
  removePost(): Observable<string> {
    const deleteUrl = this.exBackendURL + '/delete';
    return this.httpClient.post <string>(deleteUrl, this.highlighted);
  }
  removeElement() {
    if (this.highlighted !== null && this.changeable === null) {
      this.removePost().subscribe();
      this.updateView();
      this.makeNull();
    }
  }
  editElement() {
    if (this.changeable === null && this.highlighted !== null) {
      this.changeable = this.highlighted;
      this.prevElemValue =  this.nodeList.find(x => x.id === this.highlighted).node.slice(0, this.nodeList.find(x => x.id === this.highlighted).node.length);
    }
  }


}



